demarrer_requete:
    ld   [%sp+1],%a	// le descripteur de la requête
    ...			// envoyer le numéro de secteur à lire
    out  ...,[50]	// démarrer la lecture
    rtn

#define FLAG_IE	0x100    // bit 8 dans SR

masquer_interruptions:	 // retirer le bit IE de SR
    and  ~FLAG_IE,%sr	 // sr $\leftarrow$ sr AND tous les bits à 1 sauf IE
    rtn

demasquer_interruptions: // ajouter le bit IE à SR
    or   FLAG_IE,%sr	 // sr $\leftarrow$ sr OR bit IE à 1
    rtn
