#define KBD_REG_STATUS		20
#define KBD_REG_DATA		21

#define KBD_STA_READY           0x01		// cf doc du contrôleur

lire_clavier:
// attendre qu'une touche soit appuyée : boucler tant que bit Ready $\neq$ 1
	in   [KBD_REG_STATUS],%a		// a $\leftarrow$ statut du contrôleur
	and  KBD_STA_READY,%a			// a $\leftarrow$ a $\wedge$ 0x01
	cmp  0,%a				// comparer 0 à a
	jeq  lire_clavier			// boucle si a = 0 (rien à lire)
// lire la touche
	in   [KBD_REG_DATA],%a			// a $\leftarrow$ code de la touche
	rtn
