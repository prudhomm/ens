% Pour vérifier qu'on écrit du LaTeX moderne
% \RequirePackage [orthodox] {nag}

\documentclass [twoside,openright,a4paper,11pt,french] {report}

    % Règles de typographie françaises
    \usepackage[francais]{babel}

    % Jeu de caractères UTF-8
    \usepackage[utf8]{inputenc}
    \usepackage[T1]{fontenc}

    % Inclure la bibliographie dans la table des matières comme une section
    \usepackage [numbib] {tocbibind}	% numbib : section numérotée

    % Fonte élégante
    \usepackage{mathpazo}
    \usepackage [scaled] {helvet}
    \usepackage{courier}

    % pour \EUR
    \usepackage{marvosym}

    % \usepackage{emptypage}

    % Utilisation de tableaux
    \usepackage{tabularx}

    % Utilisation d'url
    \usepackage{hyperref}
    \urlstyle{sf}

    % Utilisation d'images
    \usepackage{graphicx}
    \setkeys{Gin} {keepaspectratio}	% par défaut : conserver les proportions

    % Définition des marges
    \usepackage [margin=25mm, foot=15mm] {geometry}

    \parskip=2mm
    \parindent=0mm

    \pagestyle{plain}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Page de garde
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\thispagestyle{empty}

\begin{center}
    % le ".1" est juste là pour montrer qu'on peut mettre des
    % dimensions non entières...
    \includegraphics [width=8.1cm] {logo-ufr.pdf}       

    \vfill

    {
	\large
	\textsc{
	    Master d'informatique \\
	    Parcours SIRIS \\
	    Science et Ingénierie des Réseaux, de l'Internet et des Systèmes
	}
    }

    \bigskip\bigskip
    \bigskip\bigskip

    {\huge Travail d'Étude et de Recherche}

    \bigskip\bigskip

    % Identité de l'auteur
    {\large Jacques \textsc{Sélère}}

    % Contact mail ou téléphone   
    {\small jacques.selere@etu.unistra.fr}

    \vfill

    % Titre du TER : mettez un titre utile
    {
	\huge
	\textsc{
	    Comment faire un bon \\
	    ~ \\
	    mémoire de TER ?
	}
    }

    \vfill\vfill

    {\large TER encadré par}

    \medskip

    % Identité de l'encadrant
    {\large Jean \textsc{Breille}}

    % Contact mail ou téléphone
    {\small jbreille@unistra.fr}

    \bigskip

    \bigskip

    % Logo de votre structure d'accueil
%    \includegraphics [height=2.5cm] {logo-entreprise.pdf}       

    % Supprimez le reste de cette page si vous vous servez de ce
    % fichier source comme modèle
    \vfill
    \begin{center}
	\tiny
	\copyright Pierre David,
	% avec des contributions de 
	% Cristel Pelsser, Stéphane Cateloin et Vincent Loechner

	Disponible sur \url{http://gitlab.com/pdagog/ens}.

        Ce texte est placé sous licence « Creative Commons Attribution
	-- Pas d’Utilisation Commerciale 4.0 International » \\
	Pour accéder à une copie de cette licence,
	merci de vous rendre à l'adresse suivante
	\url{http://creativecommons.org/licenses/by-nc/4.0/}

	\includegraphics [scale=.5] {by-nc}
    \end{center}
    % Supprimez jusqu'ici


\end{center}

% Page blanche au dos de la page de garde
%\cleardoublepage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table des matières
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

{
    \parskip=0pt
    \tableofcontents
}

% Page blanche entre la table des matières et le texte
\cleardoublepage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Chapitre 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Introduction}
    \label{chap:intro}

Le Travail d'Étude et de Recherche (TER) constitue une unité
d'enseignement importante de votre master d'informatique, correspondant
à 6 crédits ECTS, ce qui signifie d'une part que l'obtention d'une
bonne note est cruciale pour votre réussite au master, et d'autre part
que le TER nécessite un travail conséquent. Celui-ci est estimé à
environ 150~heures de travail personnel, soit environ 12 heures par
semaine sur l'ensemble du semestre.

Le mémoire de TER constitue donc un élément essentiel : il doit
permettre de mettre en valeur votre compréhension du sujet et votre
contribution, qui doit être originale et personnelle.

Votre mémoire sera lu par un «~rapporteur~», c'est-à-dire un
enseignant de l'équipe pédagogique (donc une personne ayant des
compétences dans votre discipline), dont la mission est d'évaluer votre
compréhension du contexte et de la problématique, ainsi que votre
contribution.

Ce document a pour but de rappeler les éléments attendus par le
rapporteur pour évaluer votre travail. Il est de votre intérêt de
bien lire ce document et fournir tous les éléments.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Chapitre 2 : le plan
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Le plan}
    \label{chap:plan}

\section{Critères de notation}

Votre mémoire sera jugé sous plusieurs critères (cf grille de notation
en annexe~\ref{anx:bareme}, page~\pageref{anx:bareme}), notamment~:

\begin{itemize}
    \item le contexte dans lequel s'inscrit le TER~: par exemple, si
	le sujet de votre TER est de trouver une nouvelle méthode de
	compression de foobar, il faut expliquer ce qu'est la compression
	de foobar, à quoi elle peut servir, et les différentes méthodes
	existantes et en quoi elles sont insuffisantes.

    \item le problème à résoudre : quel est le problème et en quoi la
	résolution de celui-ci est utile. Par exemple, ce que peut
	apporter une nouvelle méthode de compression de foobar et en
	quoi celle-ci peut sauver l'humanité, ou plus modestement à
	faire avancer la compression de foobaz qui elle-même est basée
	sur la compression de foobar.

    \item votre contribution personnelle~: il s'agit de la partie la
	plus importante de votre TER, celle sur laquelle vous avez passé
	le plus de temps. On attend de vous un travail original, étayé
	et argumenté. Il n'est pas question de résumer ou synthétiser
	un ou plusieurs articles, et encore moins de traduire des extraits
	d'un article, mais d'avoir une vraie valeur ajoutée dont la
	nature dépend du sujet : une implémentation, des évaluations,
	une reproduction d'expérience, un état de l'art d'un sujet, etc.

	Par le passé, nous avons constaté que des étudiants se sont
	contenté de traduire des fragments d'articles, ce qui ne
	constitue pas une contribution personnelle

    \item enfin, la mise en perspective de votre travail~: les avancées,
	ou à contrario les limites de votre travail, les pistes
	d'amélioration ou les utilisations de votre méthode pour
	rédoudre un autre problème majeur (par exemple l'utilisation
	de votre méthode pour améliorer la décompression de foobar).

\end{itemize}

\section{Votre plan}
    \label{sec:plan}

Le plan de votre mémoire est libre, il ne doit pas forcément être
structuré suivant les items ci-dessus, mais il doit faire apparaître
clairement votre démarche et les points évoqués ci-dessus. Certains
(par exemple la mise en perspective) peuvent faire l'objet de quelques
paragraphes dans la conclusion. Les enchaînements entre les chapitres
et les sections doivent être compréhensibles.

N'omettez pas la table des matières, et numérotez vos chapitres et
vos sections (1, 1.1, 1.1.1, etc.), cela aide à se repérer. Et bien
évidemment, numérotez les pages, sinon ça ne sert à rien.

\section{Le plagiat}

\begin{figure}[htbp]
    \begin{center}
	\includegraphics[width=0.8\linewidth]{choc-nobel}
    \end{center}
    \caption{Corrélations entre pays suivant le nombre de lauréats du
	prix Nobel pour 10 millions d'habitants et (A) consommation
	de thé par habitant, (B) consommation de vin par habitant,
	(C) nombre de magasins Ikea pour 10 millions d'habitants et (D)
	PIB par habitant. Corrélation entre pays (E) suivant la
	consommation de chocolat et le PIB par habitant
	(extrait de \cite{maurage2013})}
    \label{fig:choc-nobel}
\end{figure}

Il est rappelé que le plagiat est sanctionné par la loi, par
l'université et par vos rapporteurs : si les courtes citations sont
autorisées (une ou deux phrases maximum, quelques figures difficiles
à reproduire comme par exemple la figure~\ref{fig:choc-nobel} extraite
de \cite{maurage2013}), vous devez en donner la source.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Chapitre 3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{La forme}
    \label{chap:forme}

Un bon mémoire allie un fond de qualité et une forme parfaite. Quelques
règles de bon sens s'appliquent.

\section{Taille}

Votre mémoire doit faire 20 pages, non compris les annexes.


\section{Typographie}

La rédaction de textes français obéit à des règles précises en
Français : cela s'appelle la «~typographie~» \cite{andre1990} et il
est intéressant de s'en imprégner pour éviter des erreurs grossières
et donner à votre document un aspect de qualité.

\section{L'orthographe et la grammaire}

L'orthographe et la grammaire sont des prérequis indispensables pour
la rédaction du mémoire. Si vous n'êtes pas sûr de vous, faites-vous
relire par un tiers. C'est dommage de perdre des points sur ce critère.

\section{Le style}

Même si votre mémoire n'a pas comme objectif de décrocher le prix
Goncourt, vous devez faire attention au style :

\begin{itemize}
    \item faites des phrases construites, avec des verbes. Exemple vécu,
	à éviter : «~Il y a des problèmes. Par exemple le
	format~» (il n'y pas de verbe)~;

    \item ne parlez pas au lecteur. Exemple vécu, à éviter : «~je vais
	vous présenter~»~;

    \item employez une forme active : plutôt qu'écrire «~xyz a été
	réalisé~», utilisez «~j'ai réalisé xyz~»~;

    \item soyez précis : écrivez «~j'ai réalisé xyz~» plutôt que «~on a
	réalisé xyz~». N'oubliez pas que le rapporteur doit évaluer
	\textbf{votre} travail (et pas celui de votre encadrant ou
	d'autres auteurs)~;

    \item évitez le futur : tout ce qui est fait au moment de l'écriture
	du mémoire doit être rédigé au passé ou au
	présent. Réservez le futur pour ce qui n'est pas encore
	réalisé ou pour les perspectives.

\end{itemize}


\section{Numérotez}

Numérotez tout ce qui peut l'être : pages, chapitres, sections, figures,
tables, bibliographie. Laissez à votre logiciel le soin de numéroter
automatiquement, il le fera mieux que vous «~manuellement~». Utilisez
des références si vous devez mettre en relation plusieurs éléments
de votre discours (exemple fictif : voir la figure~\ref{fig:choc-nobel},
page~\pageref{fig:choc-nobel}, ou encore le chapitre~\ref{chap:plan} et
plus spécifiquement la section~\ref{sec:plan}, page~\pageref{sec:plan}).


\section{Les illustrations}

Un petit dessin valant mieux qu'un grand discours, les schémas sont
souvent appréciés par le rapporteur, qui ne connaît pas forcément
votre contexte aussi bien que vous.  Quelques règles cependant sont à
respecter :

\begin{itemize}
    \item numérotez et légendez vos figures (voir figure~\ref
	{fig:choc-nobel}, page~\pageref{fig:choc-nobel}, par exemple) ;
    \item référencez vos figures : une figure non référencée dans le
	texte ne sert à rien ;
    \item expliquez vos figures dans le texte : si une figure n'a pas
	besoin d'explication, cela signifie qu'elle est sans doute trop
	simple et n'a donc pas besoin de figurer dans votre mémoire ;
    \item citez la provenance de vos figures, si elles ne sont pas de
	vous : il est admis d'utiliser des figures réalisées par
	d'autres, si vous citez en la provenance ;
    \item les logos de logiciels ou de produits sont à bannir : ils
	n'apportent rien à la compréhension de votre mémoire et ne font
	que prendre de la place inutile que vous pourriez utiliser pour
	mieux présenter votre contribution ;
    \item privilégiez (sauf peut-être pour les photos) un format
	«~vectoriel~» à un format «~bitmap~» : le bitmap prend
	de la place, peut être lent à s'afficher ou à s'imprimer,
	et ne permet pas de zoomer facilement ;
    \item vérifiez que vos figures sont lisibles lorsque vous imprimez
	votre mémoire, y compris lorsque vous imprimez sur une
	imprimante noir et blanc.
\end{itemize}

Les copies d'écran n'apportent généralement pas grand-chose : elles
contiennent trop d'informations inutiles et sont peu lisibles. De plus,
le message véhiculé est souvent très succinct, voire trop léger. Un
fichier de configuration, une commande ou son résultat doivent être
présentés, si c'est vraiment nécessaire, comme du texte et non comme
une copie d'écran.

\section{Le niveau de détail}

Trouver le bon niveau de détail est souvent facilité par les contraintes
qui vous sont imposées quant au nombre de pages.

Cependant, même si vous avez la place, évitez les recopies de code ou de
commandes. Si vous voulez vraiment en faire, commentez-les dans le texte
et évitez de faire des erreurs dans les recopies ! Ne les modifiez pas
dans le texte (sauf pour supprimer une sortie trop longue par exemple,
dans ce cas remplacez la partie supprimée par «~[...]~»). Évitez
les codes trop longs~: utilisez des annexes si nécessaire.


\section{La bibliographie}

La bibliographie constitue une partie importante de votre mémoire,
car c'est la base sur laquelle se construisent les avancées
scientifiques. Elle constitue un critère de qualité du travail
(avez-vous trouvé les bonnes sources ? les documents sur lesquels vous
vous appuyez sont-ils sérieux ?).

La bibliographie~\cite{savoirs2010} vient en annexe, elle doit donner
tous les renseignements nécessaires pour permettre au lecteur de
retrouver les documents concernés : auteur, titre du document ou de
l'ouvrage, éditeur, année de publication, URL si nécessaire, date de
consultation pour un site Web, etc.

Chaque document dans la bibliographie comporte une référence (un
numéro, une abréviation ou autre), que vous devez citer dans le texte :
un document non cité ne devrait pas apparaître dans la bibliographie.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Conclusion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Conclusion}
    \label{chap:conc}

Il est maintenant temps de rédiger votre mémoire. Puissent ces quelques
conseils vous guider afin de vous aider à présenter le mieux possible
tout le travail que vous avez effectué durant votre TER !


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Annexe : barême
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\appendix
\chapter{Barême}
    \label{anx:bareme}

\vspace*{-20mm}	% on triche

\begin{center}
    \includegraphics[height=.8\textheight]{bareme.pdf}

    \vspace* {-10mm} % on triche encore

    Source : Pascal Schreck
\end{center}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Annexe : soutenance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{La soutenance}

Même si ce document concerne en priorité votre mémoire de TER,
il n'est pas inutile de rappeler quelques conseil de bon sens
pour que votre soutenance se déroule le mieux possible.

\begin{enumerate}
    \item Ne reproduisez pas le mémoire dans votre présentation : vous
	n'avez ni la place, ni le temps. Détachez-vous du mémoire et
	repartez de zéro pour construire un nouveau discours tenant
	compte de la contrainte de temps.

    \item Travaillez sur les idées et les messages que vous voulez
	faire passer. Comptez une idée par diapo. Explicitez les idées,
	ne vous contentez pas de les suggérer.

    \item Ne surchargez pas le texte de votre présentation : ne faites
	pas de phrases, insistez plutôt sur quelques mots pour exposer
	vos idées.

    \item Si vous pouvez prendre des libertés avec la grammaire et
	ne pas mettre de phrases, vous n'êtes pas dispensé de respecter
	l'orthographe.

    \item Adoptez un fond sobre pour ne pas perturber votre message.

    \item Ne lisez surtout pas les diapos que vous présentez ou, pire
	encore, un texte que vous auriez préparé.

    \item Faites des illustrations (schémas, figures, courbes) qui
	puissent être lues à plusieurs mètres de distance. N'hésitez
	pas à prendre des libertés avec votre style de présentation
	pour faire une figure en pleine page.

    \item Attention aux contrastes : votre présentation projetée dans
	une salle éclairée aura un contraste beaucoup moins bon que
	l'écran de votre portable. Évitez donc les couleurs pâles
	sur fond clair, ou les couleurs peu foncées sur fond sombre.

    \item Une de vos missions est de maintenir l'attention de votre
	auditoire. Pensez que les membres du jury ont déjà peut-être
	une dizaine de présentations à leur actif, ainsi qu'un bon
	repas...  Vous devez les motiver pour vous écouter.

    \item Respectez la durée de votre présentation. Ne terminez pas
	trop en avance (vous n'avez donc rien à dire ?), ne terminez pas
	trop en retard (vous ne savez pas synthétiser et tenir compte
	d'une contrainte ?).

    \item Répétez. Répétez. Répétez. Répétez. Répétez. Répétez.
	Répétez. Répétez. Répétez. Répétez. Répétez. Répétez.

    \item Lors de la séance des questions, laissez les membres du jury
	aller jusqu'au bout de leurs questions, sans les
	interrompre. N'hésitez pas à prendre quelques secondes pour
	vous permettre de réfléchir à chaque question, voire de la
	reformuler pour vérifier que vous l'avez bien comprise.

\end{enumerate}

Si vous n'avez pas l'habitude de faire des présentations, vous trouverez
facilement un grand nombre de vidéos ou de tutoriels qui vous donneront
de bons conseils.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bibliographie
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\cleardoublepage
\bibliographystyle{plain}
\bibliography{mem-ter}

\end{document}
